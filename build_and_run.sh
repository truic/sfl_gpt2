#!/usr/bin/env bash
BASEDIR=$(dirname "$0");
sudo docker build --no-cache -t sfl_model:latest $BASEDIR;
docker run --rm -d -p 5000:5000 --name sfl_model sfl_model:latest;