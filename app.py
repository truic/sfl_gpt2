from inference import Inference

from flask import Flask, request, abort
import requests
import json
import toml
import os

app = Flask(__name__)
#load model and it's configuration via the config files 
model = Inference(toml.load(os.path.join("config", "model_inference.toml")), toml.load(os.path.join("config", "industry_id.toml")))

@app.route('/generate_names', methods = ['POST'])
def generate_names():
    """POST request function.

    Parameters
    ----------
    Keywords: str 
        keywords seperated by spaces
    Industry: int
        An industry id. Referenced by config/industry_id.toml
    Location: str
        contains the location for the business
    NumResults: int
        the number of results that should be generated (may not be the returned amount, due to clean up of duplicates and empty strings)
    RandomSeed: int
        a number used to initialize the generator before results are generated

    Returns
    -------
    JSON Object with following fields:
    
    Names: arr
        An array of strings 
    """
    try: #load post data
        message = json.loads(request.data)
    except json.JSONDecodeError: #return 400 data if unable to load data
        print(e)
        return abort(400) 

    try: #generate names
        if message.get("RandomSeed", None) != None: #if RandomSeed is given, reset model seed
            model.reset_random_seed(message["RandomSeed"])
        names = model.get_result_list(message["Keywords"], message["Industry"], message["Location"], message["NumResults"]) #generate results
    except Exception as e: #if generation fails return 500 error
        print(f"An error occurred while trying to generate names: {e}") 
        return abort(500) 
    #return generated names 
    return_message = {   
        "Names" : names 
    }

    return return_message

if __name__ == '__main__':
    app.run(host="0.0.0.0") #run on default (5000) port