FROM nvcr.io/nvidia/pytorch:20.12-py3 

RUN mkdir src
WORKDIR src/
COPY . .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt 

#EXPOSE 80
CMD [ "python", "app.py" ]